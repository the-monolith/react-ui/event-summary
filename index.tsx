import { component, React, Style } from 'local/react/component'
import { Tooltip } from 'local/react-ui/tooltip'

export const defaultEventSummaryStyle: Style = {
  color: 'inherit',
  textDecoration: 'none',
  position: 'relative',
  display: 'inline-block'
}

export const defaultEventSummaryLineStyle: Style = {
  display: 'block'
}

export const EventSummary = component
  .props<{
    style?: Style
    lineStyle?: Style
  }>({
    style: defaultEventSummaryStyle,
    lineStyle: defaultEventSummaryLineStyle
  })
  .render(({ lineStyle, style }) => <span />)
